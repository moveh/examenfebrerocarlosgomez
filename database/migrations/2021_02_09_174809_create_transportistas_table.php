<?php

use App\Models\Empresa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransportistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('transportistas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellidos');
            $table->string('slug');
            $table->date('permisoConducir');
            $table->string('imagen');
            $table->timestamps();
        });

        Schema::create('paquetes', function (Blueprint $table) {
            $table->id();
            $table->string('direccion');
            $table->boolean('entregado');
            $table->string('imagen');
            $table->timestamps();
        });

        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('transportistas_empresas', function (Blueprint $table) {
            $table->primary('id')->increments();
            $table->foreign('transportistas_id')->references('id')->on('transportistas')->onDelete('cascade');
            $table->foreign('empresas_id')->references('id')->on('empresas')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportistas');
    }
}
