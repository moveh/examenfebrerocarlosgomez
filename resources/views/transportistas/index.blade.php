@extends("layouts.master")

@section("titulo")
    Inicio
@endsection

@section("contenido")
    <div class="row">
        @foreach( $transportistas as  $transportista )
            <div class="col-xs-12 col-sm-6 col-md-4 mb-4">
                <div class="card text-white bg-dark" style="width: auto;">
                    <a href="{{ route('transportistas.show' , $transportista->id) }}">
                        <img class="card-img-top" src="{{asset('assets/img')}}/{{$transportista->imagen}}" alt="{{$transportista->nombre}}"
                             style="height:400px"/>
                    </a>
                    <div class="card-body">
                        <h4 class="card-title text-center">{{$transportista->nombre}}</h4>
                    </div>
                    <ul class="list-group list-group-flush text-dark">
                        <p>Paquetes pendientes de entrega</p>
                    </ul>
                </div>
            </div>
        @endforeach
    </div>
@endsection