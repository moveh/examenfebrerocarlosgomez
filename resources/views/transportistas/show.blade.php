@extends('layouts.master')

@section('titulo')
    Paquetería
@endsection

@section("contenido")
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-1">
                <div class="mt-5 divImagen">
                    <div class="card-body">
                        <img class="card-img-top img-fluid" src="{{asset('assets/imagenes/transportistas')}}/{{$transportista->imagen}}">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card mb-3 mt-5 text-dark">
                    <div class="card-body border border-danger">
                        <h4 class="card-title text-center text-danger">{{$futbolista->nombre}}</h4>
                        <ul class="list-group list-group-flush text-dark">
                            <li class="list-group-item">Pierna: {{$futbolista->pierna}}</li>
                            <li class="list-group-item">Peso: {{$futbolista->peso}}kg</li>
                            <li class="list-group-item">Altura: {{$futbolista->altura}}cm</li>
                        </ul>
                        <h5 class="card-title">Dorsal</h5>
                        <p class="card-text">{{$futbolista->dorsal}}</p>
                        <a href="{{route('futbolistas.index')}}" class="btn btn-primary">Inicio</a>
                        <a href="{{route('futbolistas.edit', $futbolista->id)}}" class="btn btn-dark">Editar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection