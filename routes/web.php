<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\TransportistaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InicioController::class, 'inicio'])->name('home');

Route::get('transportistas', [TransportistaController::class, 'index'])->name('transportistas.index');

Route::get('paquetes/crear', [TransportistaController::class, 'crear'])->name('paquetes.crear');

Route::get('transportistas/{transportista}', [TransportistaController::class, 'show'])->name('transportistas.show');

Route::get('transportistas/{transportista}/entregar', [TransportistaController::class, ''])->name('transportistas.');

Route::get('transportistas/{transportista}/noentregado', [TransportistaController::class, ''])->name('transportistas.');


