<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transportista extends Model
{
    protected $table = 'transportistas';
    use HasFactory;

    public function transportista(){
        return $this->belongsToMany(Empresa::class);
    }
}
