<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transportista;

class TransportistaController extends Controller
{
    //

    function index()
    {
        $transportistas = Transportista::all();
        return view('transportistas.index')->with('transportistas', $transportistas);
    }

    function show(Transportista $transportista)
    {

        return view('transportistas.show', ['transportista' => $transportista]);
    }

}
